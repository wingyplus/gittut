package { 'tomcat7':
ensure => installed
}

package { 'vim':
  ensure => installed
}
